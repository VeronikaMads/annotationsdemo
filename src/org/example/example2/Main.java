package org.example.example2;

public class Main {
    public static void main(String[] args) {
        Dog tor = new Dog();
        Cat kitty = new Cat();
        tor.speak();
        kitty.speak();
        inspectService(Animal.class);
        inspectService(Cat.class);
        inspectService(Dog.class);
    }
    public static void inspectService (Class <?> animalsService){
        if (animalsService.isAnnotationPresent(AnimalsService.class)) {
            AnimalsService ann = animalsService.getAnnotation(AnimalsService.class);
            System.out.println(ann.name() + ann.annotationType());
        } else {
            System.out.println("Annotation not found");
        }
    }
}
