package org.example.example2;

import java.lang.annotation.*;
@Documented
@Target(ElementType.TYPE) // поле действия - метод
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AnimalsService {
    String name();
    double age ();
}
