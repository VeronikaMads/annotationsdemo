package org.example.example1;
@Service(name = "VerySimpleService",lazyLoad = true)
public class SimpleService {
    @Init
    public void LazyInit() throws Exception {
        System.out.println(" Go lazy init");
    }
    public void LazyInit2() throws Exception {
        System.out.println(" Go lazy init without @Init");
    }
}
