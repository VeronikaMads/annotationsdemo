package org.example.example1;

import java.lang.annotation.*;

@Documented
// аннотация будет присутствовать в javaDoc(стандарт для документирования классов Java.)
@Retention(RetentionPolicy.RUNTIME)
//жизненный цикл (на стадии компиляции/выполнения кода)
@Target(ElementType.TYPE)
//область применения (класс/поле/метод/аргумент)
@Inherited
// будет наследоваться нашими потоками(аннотация родителя уноследуеться всеми его потоками)
public @interface Service { // обьявление аннотации
    String name (); // обязательное свойство аннотации типа String
    boolean lazyLoad() default  false; // "default" - необязательное значение свойства (по умолчанию false)

}
